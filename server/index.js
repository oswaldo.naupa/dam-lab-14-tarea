const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const cors = require('cors');

//clase usuarios
const {addUser, removeUser,getUser, getUsersInRoom} = require('./users.js');

const PORT = process.env.PORT  || 5000;
//router
const router = require('./router');


const app = express();

const server = http.createServer(app);
const io = socketio(server);

//sockets
io.on('connect', (socket) => {
  var fecha = new Date();
  var hora = fecha.getHours()+":"+("0"+fecha.getMinutes()).slice(-2)
    socket.on('join', ({ name, room }, callback) => {
      const { error, user } = addUser({ id: socket.id, name, room });
  
      if(error) return callback(error);
  
      socket.join(user.room);
  
      socket.emit('message', { user: 'admin', text: `${user.name}, bienvenido a la sala de ${user.room}.`, date:hora});
      socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} se ha unido a la sala!`, date:hora});
      console.log(`Tenemos una nueva conexion ${user.name}`);
  
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
  
      callback();
    });
  
    socket.on('sendMessage', (message, callback) => {
      const user = getUser(socket.id);
      io.to(user.room).emit('message', { user: user.name, text: message, date:hora });
  
      callback();
    });
  
    socket.on('disconnect', () => {
      const user = removeUser(socket.id);
  
      if(user) {
        io.to(user.room).emit('message', { user: 'Admin', text: `${user.name} abandono la sala.`, date:hora });
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
        console.log(`El usario ${user.name} se desconecto`);
      }
    })
    /*// Handle typing event
    socket.on('typing', function(data){
      socket.broadcast.emit('typing', data);
    });*/

  });


app.use(cors());
app.use(router);

server.listen(PORT, () => console.log(`Server corriend en el puerto ${PORT}`));
